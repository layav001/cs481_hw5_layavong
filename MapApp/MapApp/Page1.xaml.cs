﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace MapApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public ObservableCollection<string> loc { get; set; }
        public Page1()
        {
            InitializeComponent();
            Dest();
            pins(); 
        }

        public void Dest()
        {
            loc = new ObservableCollection<string>();
            
           
            loc.Add("First Home");
            loc.Add("High School");
            loc.Add("100 Thieves Compound");
            loc.Add("Staples Center");

            p1.ItemsSource = loc; 
        }

        public void pins()
        {
            var pin = new Pin()
            {
                Position = new Position(33.5879444, -117.1831103),
                Label = "Parents House",
                Address = "27564 Brentstone Way, Murrieta, CA 92563"
            };

            var pin2 = new Pin()
            {
                Position = new Position(33.5970238, -117.1674933),
                Label = "Vista Murrieta High School",
                Address = "28251 Clinton Keith Rd, Murrieta, CA 92563"
            };

            var pin3 = new Pin()
            {
                Position = new Position(34.0204719, -118.3828495),
                Label = "100 Thieves CashApp Compound",
                Address = "6050 W Jefferson Blvd, Los Angeles, CA 90016"
            };

            var pin4 = new Pin()
            {
                Position = new Position(34.0430175, -118.2694428),
                Label = "Staples Center",
                Address = "1111 S Figueroa St, Los Angeles, CA 90015"
            };
            first.Pins.Add(pin);
            first.Pins.Add(pin2);
            first.Pins.Add(pin3);
            first.Pins.Add(pin4);


        }

        private void P1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var picker = (Picker)sender;
            int select = picker.SelectedIndex;

            if(select != -1)
            {
               if (select == 0)
                    first.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.5879444, -117.1831103), Distance.FromMiles(1)));
               if (select == 1)
                    first.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(33.5970238, -117.1674933), Distance.FromMiles(1)));
               if (select == 2)
                    first.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(34.0204719, -118.3828495), Distance.FromMiles(1)));
               if (select == 3)
                    first.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(34.0430175, -118.2694428), Distance.FromMiles(1)));
            }

        }
    }
}