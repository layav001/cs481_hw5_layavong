﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MapApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Tabbed1 : TabbedPage
	{
		public Tabbed1 ()
		{
			InitializeComponent ();
            TabbedPage tabbed = new TabbedPage();
            Children.Add(new Page1());
            Children.Add(new Page2());
            Children.Add(new Page3());


        }
	}
}