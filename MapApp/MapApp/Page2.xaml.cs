﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace MapApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
        public ObservableCollection<string> loc { get; set; }
        public Page2 ()
		{
			InitializeComponent ();
            Dest();
            pins();
		}

        public void Dest()
        {
            loc = new ObservableCollection<string>();


            loc.Add("First Home");
            loc.Add("A");
            loc.Add("A");
            loc.Add("A");
            loc.Add("A");

            p2.ItemsSource = loc;
        }

        public void pins()
        {
            var pin = new Pin()
            {
                Position = new Position(33.5879444, -117.1831103),
                Label = "Parents House",
                Address = "27564 Brentstone Way, Murrieta, CA 92563"
            };

            var pin2 = new Pin()
            {
                Position = new Position(33.5970238, -117.1674933),
                Label = "Vista Murrieta High School",
                Address = "28251 Clinton Keith Rd, Murrieta, CA 92563"
            };

            var pin3 = new Pin()
            {
                Position = new Position(34.0204719, -118.3828495),
                Label = "100 Thieves CashApp Compound",
                Address = "6050 W Jefferson Blvd, Los Angeles, CA 90016"
            };

            var pin4 = new Pin()
            {
                Position = new Position(34.0430175, -118.2694428),
                Label = "Staples Center",
                Address = "1111 S Figueroa St, Los Angeles, CA 90015"
            };
            snd.Pins.Add(pin);
            snd.Pins.Add(pin2);
            snd.Pins.Add(pin3);
            snd.Pins.Add(pin4);


        }
    }
}